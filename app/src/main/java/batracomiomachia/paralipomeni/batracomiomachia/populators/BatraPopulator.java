package batracomiomachia.paralipomeni.batracomiomachia.populators;

import android.database.Cursor;

import java.util.Locale;

import batracomiomachia.paralipomeni.batracomiomachia.models.BestTime;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.DOUBLE_DOT;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.EMPTY;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.TIME_DASH;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.DB_FIELD_HOURS;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.DB_FIELD_MINS;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.DB_FIELD_SCORE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.DB_FIELD_SECS;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.DB_FIELD_TIMES;

public class BatraPopulator {
    
    public static Score cursorToScore(Cursor c) {
        Score score = new Score();

        int index = c.getColumnIndex(DB_FIELD_SCORE);
        if (index != -1) {
            score.setScore(c.getInt(index));
        }
        index = c.getColumnIndex(DB_FIELD_TIMES);
        if (index != -1) {
            score.setTimes(c.getInt(index));
        }

        BestTime bestTime = new BestTime();

        index = c.getColumnIndex(DB_FIELD_HOURS);
        if (index != -1) {
            bestTime.setHour(c.getInt(index));
        }
        index = c.getColumnIndex(DB_FIELD_MINS);
        if (index != -1) {
            bestTime.setMins(c.getInt(index));
        }
        index = c.getColumnIndex(DB_FIELD_SECS);
        if (index != -1) {
            bestTime.setSecs(c.getInt(index));
        }
        score.setBestTime(bestTime);
        return score;
    }

    public static BestTime string2BestTime(String rawBestTime) {
        String[] times = rawBestTime.split(DOUBLE_DOT);

        if(times.length <= 1) {
            return null;
        }

        BestTime bestTime = new BestTime();

        try {
            bestTime.setSecs(Integer.parseInt(times[times.length - 1]));
            bestTime.setMins(Integer.parseInt(times[times.length - 2]));
            bestTime.setHour(times.length > 2 ? Integer.parseInt(times[times.length - 2]) : 0);
        } catch (NumberFormatException e) {
            return null;
        }

        return bestTime;
    }

    public static String bestTime2String(BestTime bestTime) {
        if(bestTime == null) {
            return TIME_DASH;
        }
        return bestTime2String(bestTime.getHour(), bestTime.getMins(), bestTime.getSecs());
    }

    public static String bestTime2String(int hours, int mins, int secs) {
        String stringHours = String.format(Locale.ITALY, "%02d", hours);
        String stringMins = hours < 1 ? String.valueOf(mins) : String.format(Locale.ITALY, "%02d", mins);
        String stringSecs = String.format(Locale.ITALY, "%02d", secs);
        return (hours > 0 ? stringHours + ":" : EMPTY) + stringMins + ":" + stringSecs;
    }
}
