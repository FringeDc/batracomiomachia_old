package batracomiomachia.paralipomeni.batracomiomachia.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.enums.Country;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;
import batracomiomachia.paralipomeni.batracomiomachia.models.User;
import batracomiomachia.paralipomeni.batracomiomachia.populators.BatraPopulator;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.CIRCLE;

public class BatraChartAdapter extends ArrayAdapter<User> {

    public BatraChartAdapter(Context context, int resourceId, List<User> users) {
        super(context, resourceId, users);
    }

    @NonNull
    @Override
    @SuppressLint({"ViewHolder", "InflateParams"})
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Context context = getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_chart, null);

        User user = getItem(position);

        if(user != null) {
            Score score = user.getScore();

            TextView tvPosition = convertView.findViewById(R.id.user_position);
            TextView tvScore = convertView.findViewById(R.id.user_score);
            TextView tvCountry = convertView.findViewById(R.id.user_country);
            TextView tvNickname = convertView.findViewById(R.id.user_name);
            TextView tvTime = convertView.findViewById(R.id.user_time);
            ImageView flag = convertView.findViewById(R.id.flag);

            Country country = user.getCountry();
            if(country == null || country.equals(Country.UNKNOWN)) {
                flag.setVisibility(View.GONE);
            } else {
                flag.setImageResource(country.getResource());
            }

            tvPosition.setText(getUserPosition(position));
            tvCountry.setText(user.getCountry().getCode());
            tvNickname.setText(user.getNickname());
            tvScore.setText(String.valueOf(score.getScore()));
            tvTime.setText(BatraPopulator.bestTime2String(score.getBestTime()));
        }

        return convertView;
    }

    private String getUserPosition(int position) {
        return (position + 1) + CIRCLE;
    }
}
