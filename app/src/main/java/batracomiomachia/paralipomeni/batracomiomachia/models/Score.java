package batracomiomachia.paralipomeni.batracomiomachia.models;

import java.io.Serializable;

public class Score implements Comparable<Score>, Serializable {

    private int score;
    private int times;
    private BestTime bestTime;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public BestTime getBestTime() {
        return bestTime;
    }

    public void setBestTime(BestTime bestTime) {
        this.bestTime = bestTime;
    }

    @Override
    public int compareTo(Score score) {
        //il miglior score è quello più ALTO
        int compareScore = Integer.valueOf(this.score).compareTo(score.score);
        if(compareScore == 0) {
            //a parità di tempo, il migliore è quello PIU' BASSO
            return score.getBestTime().compareTo(this.getBestTime());
        }
        return compareScore;
    }
}
