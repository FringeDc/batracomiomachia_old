package batracomiomachia.paralipomeni.batracomiomachia.models;

import java.util.ArrayList;
import java.util.List;

public class UserChart
{
    private List<User> userChart;

    private static final UserChart ourInstance = new UserChart();

    public static UserChart get() {
        return ourInstance;
    }

    private UserChart() {
    }

    public List<User> getUserChart() {
        if(this.userChart == null) {
            return null;
        }
        List<User> userChartTemp = new ArrayList<>(this.userChart);
        this.userChart = null;
        return userChartTemp;
    }

    public void setUserChart(List<User> userChart) {
        this.userChart = new ArrayList<>(userChart);
    }
}

