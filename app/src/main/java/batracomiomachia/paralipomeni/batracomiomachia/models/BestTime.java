package batracomiomachia.paralipomeni.batracomiomachia.models;

import java.io.Serializable;

public class BestTime implements Comparable<BestTime>, Serializable {

    private Integer hour;
    private Integer mins;
    private Integer secs;

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMins() {
        return mins;
    }

    public void setMins(Integer mins) {
        this.mins = mins;
    }

    public Integer getSecs() {
        return secs;
    }

    public void setSecs(Integer secs) {
        this.secs = secs;
    }

    @Override
    public int compareTo(BestTime bestTime) {
        long thisTime = hour*3600 + mins*60 + secs;
        long thatTime = bestTime.getHour()*3600 + bestTime.getMins()*60 + bestTime.getSecs();

        return Long.valueOf(thisTime).compareTo(thatTime);

    }
}
