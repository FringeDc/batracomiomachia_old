package batracomiomachia.paralipomeni.batracomiomachia.models;

import java.io.Serializable;

import batracomiomachia.paralipomeni.batracomiomachia.enums.Country;

public class User implements Serializable, Comparable<User> {
    private String nickname;
    private Score score;
    private Country country;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public int compareTo(User o) {
        return o.getScore().compareTo(this.score);
    }
}
