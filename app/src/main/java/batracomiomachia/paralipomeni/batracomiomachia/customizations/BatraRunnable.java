package batracomiomachia.paralipomeni.batracomiomachia.customizations;

import android.animation.ValueAnimator;

public abstract class BatraRunnable implements Runnable {

    private int value;
    private ValueAnimator animation;
    private int penalities = 0;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ValueAnimator getAnimation() {
        return animation;
    }

    public void setAnimation(ValueAnimator animation) {
        this.animation = animation;
    }

    public int getPenalities() {
        return penalities;
    }

    public void setPenalities(int penalities) {
        this.penalities = penalities;
    }
}
