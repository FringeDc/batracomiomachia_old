package batracomiomachia.paralipomeni.batracomiomachia.customizations;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraService;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.EMPTY;

public class BatraTextView extends android.support.v7.widget.AppCompatTextView {

    private static final int FADE_DURATION = 100;

    private boolean isCurrent;
    private boolean isPrevious;
    private boolean writable;
    private int row;
    private int col;
    private int value;

    private Animation in;
    private Animation out;

    private ValueAnimator animator;

    public BatraTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        addCustomInfos(context, attrs, defStyle);
    }

    public BatraTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        addCustomInfos(context, attrs, 0);
    }

    private void addCustomInfos(Context context, AttributeSet attrs, int defStyle) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BatraTextView, defStyle, 0);
        int batraCol = typedArray.getInt(R.styleable.BatraTextView_batra_col, -1);
        int batraRow = typedArray.getInt(R.styleable.BatraTextView_batra_row, -1);
        typedArray.recycle();

        this.writable = true;
        this.row = batraRow;
        this.col = batraCol;
        this.value = -1;
        this.setClickable(false);

        this.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        this.setTextColor(Color.BLACK);
        this.setTextSize(16);

        in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(FADE_DURATION);

        out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(FADE_DURATION);

        super.setId(calculateId(row, col));
    }

    private int calculateId(int row, int col) {
        return (row * 10) + col;
    }

    @Override
    public void setHeight(int height) {
        setWidth(height);
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
        super.setHeight(width);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final Layout layout = getLayout();
        if (layout != null && layout.getLineCount() > 1) {
            final float textSize = getTextSize();
            setTextSize(TypedValue.COMPLEX_UNIT_PX, (textSize - 1));
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BatraTextView)) {
            return false;
        }

        BatraTextView batraTV = (BatraTextView) obj;

        return batraTV.getValue() == this.getValue() &&
                batraTV.getCol() == this.getCol() &&
                batraTV.getRow() == this.getRow();
    }

    public void setText(String text) {
        if (text.isEmpty()) {
            return;
        }
        super.setText(text);

        this.writable = false;
        this.isCurrent = true;
        this.setClickable(false);

        try {
            int intValue = Integer.parseInt(text);
            this.setValue(intValue);
        } catch (NumberFormatException e) {
            this.setValue(-1);
        }
    }

    public void clean() {
        super.setText(EMPTY);
        this.isCurrent = false;
        this.writable = true;
        this.setClickable(false);
        this.value = -1;
    }

    public void setFadedBackgroundColor(int color) {
        this.startAnimation(out);
        super.setBackgroundColor(color);
        this.startAnimation(in);
    }

    public void startHighliting(final Activity activity) {

        Context context = activity.getApplicationContext();

        int colorStart = context.getResources().getColor(R.color.colorMoves);
        int colorEnd = BatraService.getOriginalColor(context, this);
        animator = ObjectAnimator.ofInt(this, "backgroundColor", colorStart, colorEnd);
        animator.setDuration(1000);
        animator.setEvaluator(new ArgbEvaluator());
        animator.setRepeatCount(Animation.INFINITE);
        animator.setRepeatMode(ObjectAnimator.REVERSE);

        new Thread() {
            @Override
            public void run() {
                activity.runOnUiThread(new BatraRunnable() {
                    @Override
                    public void run() {
                        animator.start();
                    }
                });
            }
        }.start();
    }

    public void stopHighliting() {
        if (animator != null) {
            animator.cancel();
        }
    }

    public boolean isWritable() {
        return writable;
    }

    public void setWritable(boolean writable) {
        this.writable = writable;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

    public boolean isPrevious() {
        return isPrevious;
    }

    public void setPrevious(boolean previous) {
        isPrevious = previous;
    }
}
