package batracomiomachia.paralipomeni.batracomiomachia.constants;

public class BatraConstants {

    public interface Utils {
        String ALPHA = "alpha";
    }

    public interface Chars {
        String EMPTY = "";
        String CIRCLE = "°";
        String DOUBLE_DOT = ":";
        String TIME_DASH = "--:--:--";
        String EXTRA_SCORE = "EXTRA_SCORE";
    }

    public interface Numeric {
        long PENALITY_TIME = 20;
        int MIN_NICKNAME_LENGTH = 3;
        int MAX_NICKNAME_LENGTH = 25;
    }

    public interface DataBase
    {
        String DB_SCORE = "score.db";
        String DB_TABLE_SCORE = "score";

        String DB_FIELD_SCORE = "score";
        String DB_FIELD_TIMES = "times";
        String DB_FIELD_HOURS = "hours";
        String DB_FIELD_MINS = "mins";
        String DB_FIELD_SECS = "secs";
        String DB_MAX_SCORE = "MAX(" + DB_FIELD_SCORE + ")";

        String QUERY_CREATE_SCORE_IF_NOT_EXISTS = "CREATE TABLE IF NOT EXISTS '" + DB_TABLE_SCORE + "' ('"
                + DB_FIELD_SCORE + "' INTEGER NOT NULL, '"
                + DB_FIELD_TIMES + "' INTEGER NOT NULL, '"
                + DB_FIELD_HOURS + "' INTEGER DEFAULT 0, '"
                + DB_FIELD_MINS + "' INTEGER NOT NULL, '"
                + DB_FIELD_SECS + "' INTEGER NOT NULL, "
                + "PRIMARY KEY('" + DB_FIELD_SCORE + "'))";

        String QUERY_GET_TIMES_OF_A_SCORE = "SELECT " + DB_FIELD_TIMES + " FROM " + DB_TABLE_SCORE
                + " WHERE " + DB_FIELD_SCORE + " = ?";

        String QUERY_GET_SCORE_BY_SCORE = "SELECT * FROM " + DB_TABLE_SCORE
                + " WHERE " + DB_FIELD_SCORE + " = ?";

        String QUERY_GET_MAX_SCORE = "SELECT MAX(" + DB_FIELD_SCORE + ") FROM " + DB_TABLE_SCORE;

        String QUERY_INSERT_NEW_SCORE = "INSERT INTO " + DB_TABLE_SCORE + "(" +
                DB_FIELD_SCORE + ", " + DB_FIELD_TIMES + ", "
                + DB_FIELD_HOURS + ", " + DB_FIELD_MINS + ", " + DB_FIELD_SECS + ") " +
                "VALUES (?, ?, ?, ?, ?);";

        String QUERY_UPDATE_BY_SCORE = "UPDATE " + DB_TABLE_SCORE + " SET "
                + DB_FIELD_SCORE + " = ?, "
                + DB_FIELD_TIMES + " = ?, "
                + DB_FIELD_HOURS + " = ?, "
                + DB_FIELD_MINS + " = ?, "
                + DB_FIELD_SECS + " = ? WHERE "
                + DB_FIELD_SCORE + " = ?";
    }
}
