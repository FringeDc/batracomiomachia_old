package batracomiomachia.paralipomeni.batracomiomachia.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.adapters.BatraChartAdapter;
import batracomiomachia.paralipomeni.batracomiomachia.models.User;
import batracomiomachia.paralipomeni.batracomiomachia.models.UserChart;

public class Chart extends BatraActivity {

    ListView listView;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        listView = findViewById(R.id.listView);
        textView = findViewById(R.id.no_chart);

        List<User> users = UserChart.get().getUserChart();

        if(users == null || users.isEmpty()) {
            listView.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            BatraChartAdapter adapter = new BatraChartAdapter(getApplicationContext(),
                    R.layout.item_chart, users);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_out_right);
    }
}
