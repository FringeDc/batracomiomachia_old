package batracomiomachia.paralipomeni.batracomiomachia.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.List;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.customizations.BatraRunnable;
import batracomiomachia.paralipomeni.batracomiomachia.customizations.BatraTextView;
import batracomiomachia.paralipomeni.batracomiomachia.models.BestTime;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;
import batracomiomachia.paralipomeni.batracomiomachia.populators.BatraPopulator;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraConnectionsService;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraDBService;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraService;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.EXTRA_SCORE;

public class Batracomiomachia extends BatraActivity {

    private BatraTextView current;
    private TextView timer;
    private TextView newRecord;
    private TextView score;
    private RelativeLayout field;
    private RelativeLayout rl_submit;
    private RelativeLayout rl_retry;
    private BatraRunnable connectionChecker;
    private Handler handler;
    TextView retry;
    TextView submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batracomiomachia);

        final Context context = getApplicationContext();

        field = findViewById(R.id.field);
        timer = findViewById(R.id.tv_time);
        score = findViewById(R.id.tv_score);
        retry = findViewById(R.id.tv_restart);
        submit = findViewById(R.id.tv_submit);
        newRecord = findViewById(R.id.tv_newrecord);
        AdView adView = findViewById(R.id.adView);
        rl_retry = findViewById(R.id.resetMatch);

        if (BatraConnectionsService.isNewtworkAvailable(context)) {
            MobileAds.initialize(context, getResources().getString(R.string.admob_appID));
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }

        BatraService.setBestSizeForField(this, field);
        BatraService.initNewMatch(this, field, timer);
        enableBatraTVsClick();


        View.OnClickListener restartMatch = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogConfermaEliminazione();
            }
        };
        retry.setOnClickListener(restartMatch);
        rl_retry.setOnClickListener(restartMatch);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Score score = createCurrentScore();

                if (BatraConnectionsService.isNewtworkAvailable(getApplicationContext())) {
                    Intent intent = new Intent(getApplicationContext(), Submit.class);
                    intent.putExtra(EXTRA_SCORE, score);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    boolean saved = BatraDBService.updateOrSaveScore(context, score);
                    if (saved) {
                        toastRes(R.string.score_saved);
                        finish();
                    } else {
                        toastRes(R.string.score_not_saved);
                    }
                }
            }
        });
    }

    private Score createCurrentScore() {
        timer = findViewById(R.id.tv_time);
        Score score = new Score();
        score.setScore(current.getValue());
        BestTime bestTime = BatraPopulator.string2BestTime(String.valueOf(timer.getText()));
        score.setBestTime(bestTime);
        return score;
    }

    private void enableBatraTVsClick() {
        field = findViewById(R.id.field);
        timer = findViewById(R.id.tv_time);
        newRecord = findViewById(R.id.tv_newrecord);
        List<BatraTextView> batraTVs = BatraService.getBatraTVs(field);

        final Activity activity = this;

        for (final BatraTextView batraTV : batraTVs) {
            batraTV.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!batraTV.isPrevious()) {
                        int currentValue = current.getValue();
                        batraTV.setText(String.valueOf(currentValue + 1));
                        setCurrent(batraTV);
                    } else {
                        batraTV.setCurrent(true);
                        current.clean();
                        setCurrent(batraTV);
                        timer.setTextColor(Color.RED);
                        BatraService.addPenality();
                    }
                    Score score = createCurrentScore();

                    if (BatraService.isNewRecord(getApplicationContext(), score)) {
                        newRecord.setText(res(R.string.label_new_bestScore, score.getScore()));
                        BatraService.startHighlitingNewScore(activity, newRecord);
                    } else {
                        BatraService.stopHighlitingNewScore(newRecord);
                    }
                }
            });
        }
    }

    public void setCurrent(BatraTextView current) {
        field = findViewById(R.id.field);
        rl_submit = findViewById(R.id.rl_submit);
        score = findViewById(R.id.tv_score);
        rl_retry = findViewById(R.id.resetMatch);
        submit = findViewById(R.id.tv_submit);

        BatraService.updateViewOnCurrentChange(this, field, current);
        this.current = current;

        score.setText(String.valueOf(current.getValue()));

        int moves = BatraService.getAvailableMoves(field, current);

        Score score = createCurrentScore();

        if (BatraService.isNewRecord(getApplicationContext(), score)) {
            submit.setVisibility(View.VISIBLE);
        } else {
            submit.setVisibility(View.GONE);
        }

        if (moves == 0) {
            if (current.getValue() == 100) {
                customizeMsgToCongratulations();
            } else {
                customizeMsgToCommonSubmit();
            }
            reverseSlideToLeft(rl_submit);
            slideToRight(rl_retry);
        } else {
            slideToLeft(rl_submit);
            reverseSlideToRight(rl_retry);
        }
    }

    private void customizeMsgToCongratulations() {
        submit = findViewById(R.id.tv_submit);
        retry = findViewById(R.id.tv_restart);
        rl_submit = findViewById(R.id.rl_submit);
        rl_submit.setBackgroundColor(getResources().getColor(R.color.gold));

        TextView msg = rl_submit.findViewById(R.id.submit_tab_msg);
        submit.setBackground(getResources().getDrawable(R.drawable.textview_click_gold));
        retry.setBackground(getResources().getDrawable(R.drawable.textview_click_gold));
        msg.setText(res(R.string.game_completed));
    }

    private void customizeMsgToCommonSubmit() {
        submit = findViewById(R.id.tv_submit);
        retry = findViewById(R.id.tv_restart);
        rl_submit = findViewById(R.id.rl_submit);
        rl_submit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        TextView msg = rl_submit.findViewById(R.id.submit_tab_msg);
        submit.setBackground(getResources().getDrawable(R.drawable.textview_click1));
        retry.setBackground(getResources().getDrawable(R.drawable.textview_click1));
        msg.setText(res(R.string.no_moves_available));
    }

    @Override
    public void onStop() {
        super.onStop();
        stopConnectionChecker();
    }

    @Override
    public void onResume() {
        super.onResume();
        startConnectionChecker();
    }

    private void stopConnectionChecker() {
        handler.removeCallbacks(connectionChecker);
    }

    private void startConnectionChecker() {
        handler = new Handler();
        connectionChecker = new BatraRunnable() {
            @Override
            public void run() {
                boolean newtworkAvailable = BatraConnectionsService.isNewtworkAvailable(getApplicationContext());
                TextView noConnection = findViewById(R.id.tv_noconnectioninfo);
                noConnection.setVisibility(newtworkAvailable ? View.GONE : View.VISIBLE);
                noConnection.requestLayout();
                handler.postDelayed(this, 3000);
            }
        };
        handler.post(connectionChecker);
    }

    private void showDialogConfermaEliminazione() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE: {
                        field = findViewById(R.id.field);
                        timer = findViewById(R.id.tv_time);
                        BatraService.stopHighlitingNewScore(newRecord);
                        BatraService.initNewMatch(Batracomiomachia.this, field, timer);
                        break;
                    }
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light));
        builder.setTitle(res(R.string.dialog_restart_title))
                .setMessage(res(R.string.dialog_restart_message))
                .setPositiveButton(res(R.string.yes), dialogClickListener)
                .setNegativeButton(res(R.string.no), dialogClickListener).show();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_out_right);
    }
}
