package batracomiomachia.paralipomeni.batracomiomachia.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import android.widget.Toast;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Utils.ALPHA;

public class BatraActivity extends AppCompatActivity {

    protected int TRANSITION_DEFAULT = 500;

    protected void slideToLeft(View view) {
        slideToLeft(view, null);
    }

    protected void reverseSlideToLeft(View view) {
        reverseSlideToLeft(view, null);
    }

    protected void slideToRight(View view) {
        slideToRight(view, null);
    }

    protected void reverseSlideToRight(View view) {
        reverseSlideToRight(view, null);
    }

    protected void slideToLeft(View view, Integer time) {
        if (view.getVisibility() != View.INVISIBLE) {
            moveHorizontally(view, 0, -view.getWidth(), time);
            view.setVisibility(View.INVISIBLE);
        }
    }

    protected void reverseSlideToLeft(View view, Integer time) {
        if (view.getVisibility() != View.VISIBLE) {
            moveHorizontally(view, -view.getWidth(), 0, time);
            view.setVisibility(View.VISIBLE);
        }
    }

    protected void slideToRight(View view, Integer time) {
        if (view.getVisibility() != View.INVISIBLE) {
            moveHorizontally(view, 0, view.getWidth(), time);
            view.setVisibility(View.INVISIBLE);
        }
    }

    protected void reverseSlideToRight(View view, Integer time) {
        if (view.getVisibility() != View.VISIBLE) {
            moveHorizontally(view, view.getWidth(), 0, time);
            view.setVisibility(View.VISIBLE);
        }
    }

    protected void moveHorizontally(View view, int fromX, int toX, Integer time) {
        if (time == null) {
            time = TRANSITION_DEFAULT;
        }
        TranslateAnimation animate =
                new TranslateAnimation(fromX, toX, 0, 0); // View for animation
        animate.setDuration(time);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }

    protected void fadeOutView(final View view, Integer time) {
        if (time == null) {
            time = TRANSITION_DEFAULT;
        }
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
        objectAnimator.setDuration(time);
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }
        });
        objectAnimator.start();
    }

    protected void fadeInView(final View view, Integer time) {
        if (time == null) {
            time = TRANSITION_DEFAULT;
        }
        view.setClickable(true);
        view.setVisibility(View.VISIBLE);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, ALPHA, 0f, 1f);
        objectAnimator.setDuration(time);
        objectAnimator.start();
    }

    public void toast(String msg) {
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        TextView v = toast.getView().findViewById(android.R.id.message);
        if (v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

    public String res(int resource, Object... args) {
        return String.format(res(resource), args);
    }

    public String res(int resource) {
        return getResources().getString(resource);
    }

    //Qui sei tu che devi fare attenzione a passare una stringResource eh
    public void toastRes(int resource) {
        toast(res(resource));
    }
}
