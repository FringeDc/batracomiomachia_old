package batracomiomachia.paralipomeni.batracomiomachia.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.enums.Country;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;
import batracomiomachia.paralipomeni.batracomiomachia.models.User;
import batracomiomachia.paralipomeni.batracomiomachia.populators.BatraPopulator;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraDBService;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraFBService;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.EXTRA_SCORE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Numeric.MAX_NICKNAME_LENGTH;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Numeric.MIN_NICKNAME_LENGTH;

public class Submit extends BatraActivity {

    TextView submit;
    EditText editText;
    TextView tvscore;
    TextView tvtime;
    TextView cancel;
    RelativeLayout background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);

        Bundle extras = getIntent().getExtras();
        Score score = null;
        if (extras != null) {
            score = (Score) extras.getSerializable(EXTRA_SCORE);
        }

        if (score == null) {
            toastRes(R.string.score_not_saved);
            finish();
            return;
        }

        final Score scoreToBeSaved = score;

        submit = findViewById(R.id.tv_realSubmit);
        editText = findViewById(R.id.et_nickname);
        tvscore = findViewById(R.id.tv_submitscore);
        tvtime = findViewById(R.id.tv_submittime);
        background = findViewById(R.id.null_back);
        cancel = findViewById(R.id.tv_cancel);

        tvscore.setText(String.valueOf(score.getScore()));
        tvtime.setText(BatraPopulator.bestTime2String(score.getBestTime()));

        editText.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();

                Editable text = editText.getText();
                if (text == null || text.toString().isEmpty()) {
                    toastRes(R.string.nickname_empty);
                    return;
                }

                String nickname = String.valueOf(editText.getText());
                if (nickname.length() < MIN_NICKNAME_LENGTH) {
                    toastRes(R.string.nickname_min);
                    return;
                }

                if (nickname.length() > MAX_NICKNAME_LENGTH) {
                    toastRes(R.string.nickname_max);
                    return;
                }

                User user = new User();
                user.setNickname(nickname);
                user.setScore(scoreToBeSaved);
                String currCountry = getResources().getConfiguration().locale.getCountry();
                user.setCountry(Country.getCountryFromString(currCountry));
                BatraDBService.updateOrSaveScore(context, scoreToBeSaved);
                BatraFBService.insertScore(Submit.this, user);

                Intent intent = new Intent(getApplicationContext(), Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Submit.this.overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_out_right);
            }
        });

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Submit.this.overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_out_right);
            }
        });
    }
}
