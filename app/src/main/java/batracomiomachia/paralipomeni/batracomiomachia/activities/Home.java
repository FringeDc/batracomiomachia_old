package batracomiomachia.paralipomeni.batracomiomachia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;
import batracomiomachia.paralipomeni.batracomiomachia.populators.BatraPopulator;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraConnectionsService;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraDBService;
import batracomiomachia.paralipomeni.batracomiomachia.services.BatraFBService;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Chars.TIME_DASH;

public class Home extends BatraActivity {

    private int TRANSITION_DURATION = 1000;

    private TextView play;
    private TextView infraLabel;
    private TextView chart;
    private ImageView front1;
    private ImageView front2;
    private ImageView front3;
    private ImageView front4;
    private ImageView front5;
    private ImageView front6;
    private RelativeLayout guide;
    private RelativeLayout header;
    private LinearLayout buttons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        play = findViewById(R.id.btn_play);
        infraLabel = findViewById(R.id.tv_start);
        buttons = findViewById(R.id.buttons);
        chart = findViewById(R.id.btn_chart);
        guide = findViewById(R.id.guide);
        updateBestScore();

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infraLabel.setText(res(R.string.homeText_start));
                goToClass(Batracomiomachia.class);
            }
        });

        chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BatraConnectionsService.isNewtworkAvailable(getApplicationContext())) {
                    infraLabel.setText(res(R.string.global_chart));
                    BatraFBService.getAllScores(Home.this);
                    slideOutView();
                } else {
                    toastRes(R.string.no_connection);
                }
            }
        });

        guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infraLabel.setText(res(R.string.label_help));
                goToClass(Guide.class);
            }
        });
    }

    private void updateBestScore() {
        TextView bestScore = findViewById(R.id.tv_bestscore);
        TextView bestTime = findViewById(R.id.tv_bestTime);
        Score score = BatraDBService.getBestScore(getApplicationContext());

        if(score == null || score.getBestTime() == null) {
            bestScore.setText("0");
            bestTime.setText(TIME_DASH);
            return;
        }
        bestScore.setText(String.valueOf(score.getScore()));
        bestTime.setText(BatraPopulator.bestTime2String(score.getBestTime()));
    }

    private void goToClass(final Class activity) {
        slideOutView();
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), activity);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        };
        handler.postDelayed(runnable, TRANSITION_DURATION);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateBestScore();
        slideInView();
    }

    private void prepareView() {
        front1 = findViewById(R.id.front1);
        front2 = findViewById(R.id.front2);
        front3 = findViewById(R.id.front3);
        front4 = findViewById(R.id.front4);
        front5 = findViewById(R.id.front5);
        front6 = findViewById(R.id.front6);
        header = findViewById(R.id.header);
        play = findViewById(R.id.btn_play);
        chart = findViewById(R.id.btn_chart);
        guide = findViewById(R.id.guide);
        infraLabel = findViewById(R.id.tv_start);
        buttons = findViewById(R.id.buttons);
    }

    private void slideInView() {
        prepareView();
        infraLabel.setVisibility(View.GONE);
        reverseSlideToLeft(front1, TRANSITION_DURATION);
        reverseSlideToLeft(front3, TRANSITION_DURATION);
        reverseSlideToLeft(front5, TRANSITION_DURATION);
        reverseSlideToLeft(header, TRANSITION_DURATION);
        reverseSlideToRight(front2, TRANSITION_DURATION);
        reverseSlideToRight(front4, TRANSITION_DURATION);
        reverseSlideToRight(front6, TRANSITION_DURATION);
        reverseSlideToRight(guide);
        fadeInView(buttons, TRANSITION_DURATION);
        guide.setClickable(true);
        play.setClickable(true);
        chart.setClickable(true);
    }

    private void slideOutView() {
        prepareView();
        infraLabel.setVisibility(View.VISIBLE);
        slideToLeft(front1, TRANSITION_DURATION);
        slideToLeft(front3, TRANSITION_DURATION);
        slideToLeft(front5, TRANSITION_DURATION);
        slideToLeft(header, TRANSITION_DURATION);
        slideToRight(front2, TRANSITION_DURATION);
        slideToRight(front4, TRANSITION_DURATION);
        slideToRight(front6, TRANSITION_DURATION);
        slideToRight(guide);
        fadeOutView(buttons, TRANSITION_DURATION);
        guide.setClickable(false);
        play.setClickable(false);
        chart.setClickable(false);
    }
}
