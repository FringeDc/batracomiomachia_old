package batracomiomachia.paralipomeni.batracomiomachia.services;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.activities.BatraActivity;
import batracomiomachia.paralipomeni.batracomiomachia.activities.Chart;
import batracomiomachia.paralipomeni.batracomiomachia.enums.BatraFirebaseType;
import batracomiomachia.paralipomeni.batracomiomachia.enums.Country;
import batracomiomachia.paralipomeni.batracomiomachia.models.BestTime;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;
import batracomiomachia.paralipomeni.batracomiomachia.models.User;
import batracomiomachia.paralipomeni.batracomiomachia.models.UserChart;

public class BatraFBService {

    public static void insertScore(@NonNull BatraActivity activity, User user) {
        if (user == null) {
            return;
        }
        Context context = activity.getApplicationContext();
        if (BatraConnectionsService.isNewtworkAvailable(context)) {
            Object[] params = new Object[]{user};
            BatraFBService.execute(activity, params, BatraFirebaseType.REGISTER);
        } else {
            activity.toastRes(R.string.no_connection);
        }
    }

    public static void getAllScores(@NonNull BatraActivity activity) {
        Context context = activity.getApplicationContext();
        if (BatraConnectionsService.isNewtworkAvailable(context)) {
            BatraFBService.execute(activity, null, BatraFirebaseType.GETCHART);
        } else {
            activity.toastRes(R.string.no_connection);
        }
    }


    private static void execute(@NonNull final BatraActivity activity,
                                final Object[] param, final BatraFirebaseType interactionType) {

        final Context context = activity.getApplicationContext();

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.addValueEventListener(new ValueEventListener() {

            BatraFirebaseType type = interactionType;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                switch (type) {
                    case REGISTER: {
                        if (!(param[0] instanceof User)) {
                            activity.toastRes(R.string.score_not_saved);
                            return;
                        }

                        User user = (User) param[0];
                        Score score = user.getScore();
                        String scoreValue = String.valueOf(score.getScore());

                        List<String> scoreValues = new ArrayList<>();
                        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                            scoreValues.add(dsp.getKey());
                        }

                        Set<RawUserScore> userValues = new TreeSet<>();

                        if (scoreValues.contains(scoreValue)) {

                            DataSnapshot registeredTimesforScore = dataSnapshot.child(scoreValue);
                            for (DataSnapshot dsp : registeredTimesforScore.getChildren()) {
                                userValues.add(dsp.getValue(RawUserScore.class));
                            }
                        }
                        RawUserScore rawUserScore = user2rawUserScore(user);
                        userValues.add(rawUserScore);

                        ref.child(scoreValue).setValue(new ArrayList<>(userValues));
                        activity.toastRes(R.string.score_saved);
                        break;
                    }
                    case GETCHART: {
                        List<User> users = new ArrayList<>();

                        //scorro i numeri (100, 98, 56, etc)
                        for (DataSnapshot dsp : dataSnapshot.getChildren()) {

                            Integer scoreValue = getIntegerKey(dsp.getKey());
                            if (scoreValue != null) {
                                for (DataSnapshot thisScore : dsp.getChildren())
                                {
                                    RawUserScore rawUserScore = thisScore.getValue(RawUserScore.class);
                                    User user = rawUserScore2user(rawUserScore);
                                    if (user != null) {
                                        user.getScore().setScore(scoreValue);
                                        users.add(user);
                                    }
                                }
                            }

                        }

                        Collections.sort(users);
                        UserChart.get().setUserChart(users);

                        Intent intent = new Intent(context, Chart.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        break;
                    }
                }
                type = BatraFirebaseType.COMPLETED;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                switch (type) {
                    case REGISTER: {
                        activity.toastRes(R.string.score_not_saved);
                        break;
                    }
                    case GETCHART: {
                        activity.toastRes(R.string.cannot_retrieve_chart);
                        break;
                    }
                }
                type = BatraFirebaseType.COMPLETED;
            }
        });
    }

    private static Integer getIntegerKey(String key) {
        if(key == null) {
            return null;
        }

        try {
            return Integer.parseInt(key);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    private static class RawUserScore implements Comparable<RawUserScore> {
        public String nickname;
        public String country;
        public BestTime time;

        String getNickname() {
            return nickname;
        }

        void setNickname(String nickname) {
            this.nickname = nickname;
        }

        BestTime getTime() {
            return time;
        }

        void setTime(BestTime time) {
            this.time = time;
        }

        @Override
        public int compareTo(RawUserScore rawUserScore) {
            return rawUserScore.getTime().compareTo(this.getTime());
        }

        String getCountry() {
            return country;
        }

        void setCountry(String country) {
            this.country = country;
        }
    }

    private static RawUserScore user2rawUserScore(User user) {
        Score score = user.getScore();
        RawUserScore rawUserScore = new RawUserScore();
        rawUserScore.setNickname(user.getNickname());
        rawUserScore.setTime(score.getBestTime());
        rawUserScore.setCountry(user.getCountry().getCode());

        return rawUserScore;
    }

    private static User rawUserScore2user(RawUserScore rawUserScore) {
        if(rawUserScore == null) {
            return null;
        }
        User user = new User();
        Score score = new Score();
        score.setBestTime(rawUserScore.getTime());
        user.setCountry(Country.getCountryFromString(rawUserScore.getCountry()));
        user.setNickname(rawUserScore.getNickname());
        user.setScore(score);
        return user;
    }
}
