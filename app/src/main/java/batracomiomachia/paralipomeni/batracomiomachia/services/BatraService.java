package batracomiomachia.paralipomeni.batracomiomachia.services;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import batracomiomachia.paralipomeni.batracomiomachia.R;
import batracomiomachia.paralipomeni.batracomiomachia.activities.Batracomiomachia;
import batracomiomachia.paralipomeni.batracomiomachia.customizations.BatraRunnable;
import batracomiomachia.paralipomeni.batracomiomachia.customizations.BatraTextView;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;
import batracomiomachia.paralipomeni.batracomiomachia.populators.BatraPopulator;

import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.Numeric.PENALITY_TIME;

public class BatraService {

    private static BatraRunnable timer;
    private static BatraRunnable subTimer;
    private static long time = 0L;
    private static Handler handler;
    private static ValueAnimator animator;

    private static int getBestSizeForField(Activity activity) {

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int minSize = Math.min(size.x, size.y);

        //removing margins of the field
        minSize = minSize - 20;

        //retrieving a multiple of 10
        return minSize - (minSize % 10);
    }

    public static void updateViewOnCurrentChange(Activity activity, RelativeLayout field, BatraTextView newCurrent) {
        int value = newCurrent.getValue();

        Context context = activity.getApplicationContext();

        int primaryColor = context.getResources().getColor(R.color.colorPrimary);
        int secondaryColor = context.getResources().getColor(R.color.colorSecondary);

        BatraTextView prevCurrent = getBatraTV(field, value - 1);
        List<BatraTextView> batraTVs = getBatraTVs(field);

        stopAllHighlitingThreads(batraTVs);

        List<BatraTextView> reachable = getAvailableBatraTVs(field, newCurrent);

        for (BatraTextView batraTV : batraTVs) {

            if (batraTV.equals(newCurrent)) {
                batraTV.setCurrent(true);
                batraTV.setPrevious(false);
                batraTV.setClickable(false);
                batraTV.setTypeface(batraTV.getTypeface(), Typeface.BOLD);
                batraTV.setTextColor(Color.WHITE);
                batraTV.setFadedBackgroundColor(primaryColor);

                if (batraTV.getValue() >= 12) {
                    highlightMoves(activity, field, batraTV);
                }
            } else if (batraTV.equals(prevCurrent) && batraTV.getValue() > 11) {
                batraTV.setCurrent(false);
                batraTV.setClickable(true);
                batraTV.setPrevious(true);
                batraTV.setTypeface(batraTV.getTypeface(), Typeface.BOLD);
                batraTV.setTextColor(Color.BLACK);
                batraTV.setFadedBackgroundColor(secondaryColor);
            } else {
                batraTV.setCurrent(false);
                batraTV.setPrevious(false);
                batraTV.setClickable(value >= 12 && reachable.contains(batraTV));
                batraTV.setTypeface(batraTV.getTypeface(), Typeface.NORMAL);
                batraTV.setTextColor(Color.BLACK);
                batraTV.setBackgroundColor(getOriginalColor(context, batraTV));
            }
        }
    }

    private static void stopAllHighlitingThreads(List<BatraTextView> batraTVs) {
        for (BatraTextView batraTV : batraTVs) {
            batraTV.stopHighliting();
        }
    }

    private static void highlightMoves(final Activity activity,
                                       RelativeLayout field, final BatraTextView focusedBatraTV) {
        List<BatraTextView> batraTVs = getBatraTVs(field);
        for (final BatraTextView batraTV : batraTVs) {
            if (isReachable(focusedBatraTV, batraTV)) {
                batraTV.startHighliting(activity);
                batraTV.setClickable(true);
            } else {
                batraTV.stopHighliting();
                batraTV.setClickable(false);
            }
            if (batraTV.isPrevious()) {
                batraTV.setClickable(true);
            }
        }
    }

    private static boolean isReachable(BatraTextView focusedBatraTV, BatraTextView batraTV) {
        if (!batraTV.isWritable()) {
            return false;
        }

        int focusedRow = focusedBatraTV.getRow();
        int focusedCol = focusedBatraTV.getCol();
        int reachableRow = batraTV.getRow();
        int reachableCol = batraTV.getCol();

        return (focusedCol == reachableCol && Math.abs(focusedRow - reachableRow) == 3) ||
                (focusedRow == reachableRow && Math.abs(focusedCol - reachableCol) == 3) ||
                (Math.abs(focusedCol - reachableCol) == 2 && Math.abs(focusedRow - reachableRow) == 2);
    }

    private static void voidField(RelativeLayout field) {
        List<BatraTextView> batraTVs = getBatraTVs(field);
        for (BatraTextView batraTV : batraTVs) {
            batraTV.clean();
        }
    }

    public static void setBestSizeForField(Activity activity, RelativeLayout field) {
        int bestSize = getBestSizeForField(activity);

        field.getLayoutParams().height = bestSize;
        field.getLayoutParams().width = bestSize;

        int batraTVsize = bestSize / 10;

        List<BatraTextView> batraTVs = getBatraTVs(field);

        for (BatraTextView batraTV : batraTVs) {
            batraTV.setHeight(batraTVsize);
        }
    }

    public static List<BatraTextView> getBatraTVs(RelativeLayout field) {
        List<BatraTextView> batraTVs = new ArrayList<>();

        LinearLayout mainLL = (LinearLayout) field.getChildAt(0);

        for (int row = 0; row < 10; row++) {
            LinearLayout ll = (LinearLayout) mainLL.getChildAt(row);
            for (int col = 0; col < 10; col++) {
                batraTVs.add((BatraTextView) ll.getChildAt(col));
            }
        }

        return batraTVs;
    }

    private static BatraTextView getBatraTV(List<BatraTextView> batraTVs, int row, int col) {
        for (BatraTextView batraTV : batraTVs) {
            if (batraTV.getCol() == col && batraTV.getRow() == row) {
                return batraTV;
            }
        }
        return null;
    }

    private static BatraTextView getBatraTV(RelativeLayout field, int value) {
        List<BatraTextView> batraTVs = getBatraTVs(field);
        return getBatraTV(batraTVs, value);
    }

    private static BatraTextView getBatraTV(List<BatraTextView> batraTVs, int value) {
        for (BatraTextView batraTV : batraTVs) {
            if (batraTV.getValue() == value) {
                return batraTV;
            }
        }
        return null;
    }

    public static void initNewMatch(final Activity activity,
                                    RelativeLayout field, final TextView timer) {
        voidField(field);
        final List<BatraTextView> batraTVs = getBatraTVs(field);
        timer.setVisibility(View.INVISIBLE);
        final Handler handler = new Handler();
        stopTimer();

        BatraRunnable runnable = new BatraRunnable() {
            @Override
            public void run() {
                BatraTextView batraTV = null;

                int value = this.getValue();
                switch (value) {
                    case 1: {
                        batraTV = getBatraTV(batraTVs, 0, 0);
                        break;
                    }
                    case 2: {
                        batraTV = getBatraTV(batraTVs, 3, 0);
                        break;
                    }
                    case 3: {
                        batraTV = getBatraTV(batraTVs, 6, 0);
                        break;
                    }
                    case 4: {
                        batraTV = getBatraTV(batraTVs, 9, 0);
                        break;
                    }
                    case 5: {
                        batraTV = getBatraTV(batraTVs, 9, 3);
                        break;
                    }
                    case 6: {
                        batraTV = getBatraTV(batraTVs, 9, 6);
                        break;
                    }
                    case 7: {
                        batraTV = getBatraTV(batraTVs, 9, 9);
                        break;
                    }
                    case 8: {
                        batraTV = getBatraTV(batraTVs, 6, 9);
                        break;
                    }
                    case 9: {
                        batraTV = getBatraTV(batraTVs, 3, 9);
                        break;
                    }
                    case 10: {
                        batraTV = getBatraTV(batraTVs, 0, 9);
                        break;
                    }
                    case 11: {
                        batraTV = getBatraTV(batraTVs, 0, 6);
                        break;
                    }
                    case 12: {
                        batraTV = getBatraTV(batraTVs, 0, 3);
                        break;
                    }
                    default:
                        break;
                }

                if (batraTV != null) {
                    batraTV.setText(String.valueOf(value));
                    ((Batracomiomachia) activity).setCurrent(batraTV);
                    this.setValue(value + 1);
                    handler.postDelayed(this, 50);

                    if (value == 12) {
                        restartTimer(activity, timer);
                    }
                }
            }
        };
        runnable.setValue(1);
        handler.postDelayed(runnable, 200);
    }

    private static void restartTimer(final Activity activity, final TextView tvTimer) {
        Context context = activity.getApplicationContext();
        tvTimer.setVisibility(View.VISIBLE);
        tvTimer.setText(context.getResources().getString(R.string.start_time));
        final long interval = 1000;
        time = 0;

        final ValueAnimator animator = ObjectAnimator.ofInt(tvTimer,
                "textColor", Color.RED, Color.BLACK);
        animator.setDuration(1000);
        animator.setEvaluator(new ArgbEvaluator());

        handler = new Handler();
        subTimer = new BatraRunnable() {
            @Override
            public void run() {
                time += 1;

                int penalitiesQty = timer.getPenalities();
                Long penalities = penalitiesQty * PENALITY_TIME;
                time += penalities;

                int intHours = (int) (time / 3600);
                int intMins = (int) (time / 60) % 60;
                int intSecs = (int) (time % 60);

                String tempo = BatraPopulator.bestTime2String(intHours, intMins, intSecs);

                if (penalities > 0) {
                    timer.setPenalities(Math.max(0, timer.getPenalities() - penalitiesQty));
                    animator.start();
                }
                tvTimer.setText(tempo);
                handler.postDelayed(this, interval);
            }
        };

        timer = new BatraRunnable() {
            public void run() {
                activity.runOnUiThread(subTimer);
            }
        };

        handler.postDelayed(timer, interval);

    }

    public static int getOriginalColor(Context context, BatraTextView batraTV) {
        int chessColorOdd = context.getResources().getColor(R.color.grey);
        int chessColorEven = context.getResources().getColor(R.color.light_grey);

        boolean isEvenBox = (batraTV.getCol() + batraTV.getRow()) % 2 == 0;
        return isEvenBox ? chessColorEven : chessColorOdd;
    }

    public static void addPenality() {
        if (timer == null) {
            return;
        }
        timer.setPenalities(timer.getPenalities() + 1);
    }

    public static List<BatraTextView> getAvailableBatraTVs(RelativeLayout field, BatraTextView current) {
        List<BatraTextView> reachable = new ArrayList<>();
        List<BatraTextView> batraTVs = getBatraTVs(field);
        for (final BatraTextView batraTV : batraTVs) {
            if (isReachable(current, batraTV)) {
                reachable.add(batraTV);
            }
        }
        return reachable;
    }

    public static int getAvailableMoves(RelativeLayout field, BatraTextView current) {
        return getAvailableBatraTVs(field, current).size();
    }

    private static void stopTimer() {
        if (handler != null) {
            handler.removeCallbacks(subTimer);
            handler.removeCallbacks(timer);
        }
    }

    public static boolean isNewRecord(Context context, Score score) {
        Score bestScore = BatraDBService.getBestScore(context);
        if (bestScore != null) {
            return score.compareTo(bestScore) > 0;
        }
        return score.getScore() > 12;
    }

    public static void startHighlitingNewScore(final Activity activity, final TextView newScore) {
        newScore.setVisibility(View.VISIBLE);
        Context context = activity.getApplicationContext();

        int colorStart = context.getResources().getColor(R.color.colorPrimary);
        int colorEnd = context.getResources().getColor(R.color.white);
        animator = ObjectAnimator.ofInt(newScore, "textColor", colorStart, colorEnd);
        animator.setDuration(500);
        animator.setEvaluator(new ArgbEvaluator());
        animator.setRepeatCount(Animation.INFINITE);
        animator.setRepeatMode(ObjectAnimator.RESTART);

        new Thread() {
            @Override
            public void run() {
                activity.runOnUiThread(new BatraRunnable() {
                    @Override
                    public void run() {
                        animator.start();
                    }
                });
            }
        }.start();
    }

    public static void stopHighlitingNewScore(final TextView newScore) {
        newScore.setVisibility(View.INVISIBLE);
        if (animator != null) {
            animator.cancel();
        }
    }
}
