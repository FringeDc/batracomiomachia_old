package batracomiomachia.paralipomeni.batracomiomachia.services;

import android.content.Context;

import batracomiomachia.paralipomeni.batracomiomachia.daos.BatraDao;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;

public class BatraDBService {

    private static int getBestScoreValue(Context context) {
        return BatraDao.getBestScoreValue(context);
    }

    private static Score getScore(Context context, int scoreValue) {
        return BatraDao.getScore(context, String.valueOf(scoreValue));
    }

    public static Score getBestScore(Context context) {
        int bestScore = getBestScoreValue(context);
        return getScore(context, bestScore);
    }

    public static boolean updateOrSaveScore(Context context, Score score) {
        if(score == null) {
            return false;
        }

        Score scoreAlreadyOnDB = BatraDao.getScore(context, String.valueOf(score.getScore()));

        if(scoreAlreadyOnDB == null || scoreAlreadyOnDB.getTimes() == 0) {
            return BatraDao.saveScore(context, score);
        }

        Score updatableData = new Score();
        updatableData.setScore(score.getScore());
        updatableData.setTimes(score.getTimes() +1);

        if(score.getBestTime().compareTo(scoreAlreadyOnDB.getBestTime()) < 0) {
            updatableData.setBestTime(score.getBestTime());
        } else {
            updatableData.setBestTime(scoreAlreadyOnDB.getBestTime());
        }
        return BatraDao.updateScore(context, updatableData);
    }
}
