package batracomiomachia.paralipomeni.batracomiomachia.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class BatraConnectionsService {

    public static boolean isNewtworkAvailable(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
