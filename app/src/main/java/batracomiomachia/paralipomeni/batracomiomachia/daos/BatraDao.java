package batracomiomachia.paralipomeni.batracomiomachia.daos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import batracomiomachia.paralipomeni.batracomiomachia.models.BestTime;
import batracomiomachia.paralipomeni.batracomiomachia.models.Score;
import batracomiomachia.paralipomeni.batracomiomachia.populators.BatraPopulator;

import static android.content.Context.MODE_PRIVATE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.DB_MAX_SCORE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.DB_SCORE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.QUERY_CREATE_SCORE_IF_NOT_EXISTS;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.QUERY_GET_MAX_SCORE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.QUERY_GET_SCORE_BY_SCORE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.QUERY_INSERT_NEW_SCORE;
import static batracomiomachia.paralipomeni.batracomiomachia.constants.BatraConstants.DataBase.QUERY_UPDATE_BY_SCORE;

public class BatraDao {

    private static SQLiteDatabase personalDB;

    public static int getBestScoreValue(Context context) {
        personalDB = context.openOrCreateDatabase(DB_SCORE, MODE_PRIVATE, null);
        personalDB.execSQL(QUERY_CREATE_SCORE_IF_NOT_EXISTS);

        int score = 0;
        Cursor cursor = personalDB.rawQuery(QUERY_GET_MAX_SCORE, null);
        if (cursor.moveToFirst()) {
            score = cursor.getInt(cursor.getColumnIndex(DB_MAX_SCORE));
        }
        cursor.close();
        personalDB.close();

        return score;
    }

    public static Score getScore(Context context, String scoreValue) {
        personalDB = context.openOrCreateDatabase(DB_SCORE, MODE_PRIVATE, null);
        personalDB.execSQL(QUERY_CREATE_SCORE_IF_NOT_EXISTS);

        Score score = null;
        Cursor cursor = personalDB.rawQuery(QUERY_GET_SCORE_BY_SCORE, new String[]{scoreValue});
        if (cursor.moveToFirst()) {
            score = BatraPopulator.cursorToScore(cursor);
        }
        cursor.close();
        personalDB.close();

        return score;
    }

    public static boolean saveScore(Context context, Score score) {
        if(score == null) {
            return false;
        }
        BestTime bestTime = score.getBestTime();
        if(bestTime == null) {
            return false;
        }

        personalDB = context.openOrCreateDatabase(DB_SCORE, MODE_PRIVATE, null);
        personalDB.execSQL(QUERY_CREATE_SCORE_IF_NOT_EXISTS);

        String[] params = {
                String.valueOf(score.getScore()),
                "1",
                String.valueOf(bestTime.getHour()),
                String.valueOf(bestTime.getMins()),
                String.valueOf(bestTime.getSecs())};

        personalDB.execSQL(QUERY_INSERT_NEW_SCORE, params);
        personalDB.close();
        return true;
    }

    public static boolean updateScore(Context context, Score score) {
        if(score == null) {
            return false;
        }
        BestTime bestTime = score.getBestTime();
        if(bestTime == null) {
            return false;
        }

        personalDB = context.openOrCreateDatabase(DB_SCORE, MODE_PRIVATE, null);
        personalDB.execSQL(QUERY_CREATE_SCORE_IF_NOT_EXISTS);

        String[] params = {
                String.valueOf(score.getScore()),
                String.valueOf(score.getTimes()),
                String.valueOf(bestTime.getHour()),
                String.valueOf(bestTime.getMins()),
                String.valueOf(bestTime.getSecs()),
                String.valueOf(score.getScore())};

        personalDB.execSQL(QUERY_UPDATE_BY_SCORE, params);
        personalDB.close();
        return true;
    }
}
